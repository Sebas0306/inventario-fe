import { createRouter, createWebHistory } from "vue-router";
import App from "./App.vue";

import Busqueda from './components/Busqueda.vue';
import Inicio from './components/Inicio.vue';
import Resultado from './components/Resultado.vue'
import Ingreso from './components/Ingreso.vue'
import Salida from './components/Salida.vue'
import Eliminar from './components/Eliminar.vue'


const routes = [
  {
    path: '/',
    name: 'root',
    component: App
  },
  {
    path: '/busqueda',
    name: 'busqueda',
    component: Busqueda
  },
  {
    path: '/inicio',
    name:'inicio',
    component: Inicio
  },
  {
    path: '/resultado',
    name: 'resultado',
    component: Resultado
  },
  {
    path:'/ingreso',
    name: 'ingreso',
    component: Ingreso
  },
  {
    path:'/salida',
    name: 'salida',
    component: Salida
  },
  {
    path:'/eliminar',
    name: 'eliminar',
    component: Eliminar
  },
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
